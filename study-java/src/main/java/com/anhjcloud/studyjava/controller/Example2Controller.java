package com.anhjcloud.studyjava.controller;

import com.anhjcloud.studyjava.service.Example2Service;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/foods")
public class Example2Controller {
    private final Example2Service example2Service;

    @GetMapping("/test")
    public void foodTest() {
        example2Service.foodBoy("초밥"); // example2Service의 foodBoy를 5번 호출
        example2Service.foodBoy("김밥");
        example2Service.foodBoy("피자");
        example2Service.foodBoy("라면");
        example2Service.foodBoy("계란");

        example2Service.checkBoy(); // example2Service의 checkBoy를 호출
    }
}
