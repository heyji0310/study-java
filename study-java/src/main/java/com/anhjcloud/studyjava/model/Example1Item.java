package com.anhjcloud.studyjava.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.PrimitiveIterator;

@Getter
@Setter
public class Example1Item {
    private String name;
    private LocalDate birthDay;
}
