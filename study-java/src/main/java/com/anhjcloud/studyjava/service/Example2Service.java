package com.anhjcloud.studyjava.service;

import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class Example2Service {
    private List<String> table = new LinkedList<>();

    public void foodBoy(String food) {
        table.add(food);
    }

    public void checkBoy() {
        System.out.println(table);
    }
}
