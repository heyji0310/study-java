package com.anhjcloud.studyjava.service;

import com.anhjcloud.studyjava.model.Example1Item;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
public class Example1Service {
    /**
     * 샘플 데이터를 생성한다.
     *
     * @return Example1Item 리스트
     */
    private List<Example1Item> settingData() {
        List<Example1Item> result = new LinkedList<>(); // result라는 이름의 List<Example1Item> 형식의 LinkedList 객체를 생성

        Example1Item item1 = new Example1Item(); // item1이라는 이름의 Example1Item 형식의 객체를 생성
        item1.setName("홍길동"); // item1 객체의 Name을 홍길동으로 선언한다.
        item1.setBirthDay(LocalDate.of(2022,1,1)); // item1 객체의 BirthDay를 20220101으로 선언한다.
        result.add(item1); // List<Example1Item> 형식인 result에 Example1Item 형식의 item1의 데이터를 추가한다.

        Example1Item item2 = new Example1Item(); // item2라는 이름의 Example1Item 형식의 객체를 생성
        item2.setName("홍길은"); // item2 객체의 Name을 홍길은으로 선언한다.
        item2.setBirthDay(LocalDate.of(2022,2,1)); // item2 객체의 BirthDay를 20220201으로 선언한다.
        result.add(item2); // List<Example1Item> 형식인 result에 Example1Item 형식의 item2의 데이터를 추가한다.

        Example1Item item3 = new Example1Item(); // Example1Item 리스트에 item3의 객체를 생성한다.
        item3.setName("홍길금"); // item3 객체의 Name을 홍길금으로 선언한다.
        item3.setBirthDay(LocalDate.of(2022,3,1)); // item3 객체의 BirthDay를 20220301으로 선언한다.
        result.add(item3); // List<Example1Item> 형식인 result에 Example1Item 형식의 item3의 데이터를 추가한다.

        Example1Item item4 = new Example1Item(); // Example1Item 리스트에 item4의 객체를 생성한다.
        item4.setName("홍길똥"); // item4 객체의 Name을 홍길똥으로 선언한다.
        item4.setBirthDay(LocalDate.of(2022,4,1)); // // item4 객체의 BirthDay를 20220401으로 선언한다.
        result.add(item4); // List<Example1Item> 형식인 result에 Example1Item 형식의 item4의 데이터를 추가한다.

        Example1Item item5 = new Example1Item(); // Example1Item 리스트에 item5의 객체를 생성한다.
        item5.setName("홍길천"); // item5 객체의 Name을 홍길천으로 선언한다.
        item5.setBirthDay(LocalDate.of(2022,5,1)); // // item5 객체의 BirthDay를 20220501으로 선언한다.
        result.add(item5); // List<Example1Item> 형식인 result에 Example1Item 형식의 item5의 데이터를 추가한다.

        return result; // 데이터가 채워진 result 반환한다.
    }

    /**
     * string 데이터를 생성한다.
     *
     * @return String 리스트
     */
    public List<String> getForEachTest() { // List<String>형식의 반환값을 갖는 getForEachTest의 이름의 메소드를 선언한다.
        List<Example1Item> data = settingData(); // settingData에 대한 결과를 List<Example1Item> 형식의 data에 넣어준다.
        List<String> result = new LinkedList<>(); // result라는 이름의 List<String> 형식으로 LinkedList 객체를 선언한다.

        data.forEach(item -> result.add(item.getName())); // data 안에 있는 Name의 List를 result에 각각 추가한다.

        for (Example1Item item : data) { // data 길이 만큼 실행한다.
            result.add(item.getName()); // result에 Name의 List를 추가한다.
        }

        return result; // result를 반환한다.

        // 예상결과물 : 홍길동 홍길은 홍길금 홍길똥 홍길천
        //            홍길동 홍길은 홍길금 홍길똥 홍길천
     }
}
